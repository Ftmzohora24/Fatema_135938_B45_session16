<?php

namespace App\Model;
use PDO, PDOException;

class Database{

    public $DBH;

    public function __construct()
    {
        try {
            $this->DBH = new PDO('mysql:host=localhost;dbname=atomic_project_b46', 'root', '');
            echo "Congratulation! DB Connected";

        } catch (PDOException $error) {
            echo "Hello Error!: " . $error->getMessage() . "<br/>";
            die();
        }
    }

}