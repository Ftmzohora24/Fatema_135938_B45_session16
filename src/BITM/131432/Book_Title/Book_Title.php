<?php

namespace App\Book_Title;
use App\Model\Database as DB;

class Book_Title extends DB{

    private $id;
    private $book_name;
    private $author_name;
    private $soft_deleted;


    public function setData($postData){

        if (array_key_exists("id", $postData)){
            $this->id = $postData["id"];
        }

        if (array_key_exists("bookName", $postData)){
            $this->book_name = $postData["bookName"];
        }

        if (array_key_exists("authorName", $postData)){
            $this->author_name = $postData["authorName"];
        }

        if (array_key_exists("softDeleted", $postData)){
            $this->soft_deleted = $postData["softDeleted"];
        }
    }

    public function store(){

        $dataArray = array($this->book_name, $this->author_name);
        $sql = "insert into book_title(book_name, author_name) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($dataArray);

        if($result){

            Message::message ("success! : Data has not been inserted!<br>");
        }
        else{
            echo "error!";
        }
    }

}